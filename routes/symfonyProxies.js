const express = require('express');
const router = express.Router();
const fs = require('fs');
const execSync = require('child_process').execSync;

function getCurrentConfiguration(){
  const symfonyProxiesConf = JSON.parse(fs.readFileSync(require('os').homedir() + '/.symfony/proxy.json'));

  symfonyProxiesConf.statuses = {};
  Object.keys(symfonyProxiesConf.domains).forEach(key => {
    const command = 'symfony local:server:list |grep ' + key;
    let url = null;
    let commandParts = null;
    let isActive = false;

    try {
      commandParts = execSync(command).toString().split('│');
      url = commandParts[3].split(' ').join('').split('\u001b')[1].split(';;')[1].split(' ').join('');
      isActive = commandParts[2].split(' ').join('') !== 'Notrunning';
    } catch (e) {
      url = 'http://' + key + '.' + symfonyProxiesConf.tld;
    }

    symfonyProxiesConf.statuses[key] = {
      url: url,
      isActive: isActive
    };
  });

  return symfonyProxiesConf;
}

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.json(getCurrentConfiguration());
});

router.get('/start/:symfonykey', function(req, res, next) {
  const symfonyProxiesConf = getCurrentConfiguration();
  const command = 'symfony server:start --daemon --dir=' + symfonyProxiesConf.domains[req.params.symfonykey];

  execSync(command);

  const command2 = 'symfony local:server:list |grep ' + req.params.symfonykey;
  let url = null;
  let commandParts = null;
  let isActive = false;

  try {
    commandParts = execSync(command2).toString().split('│');
    url = commandParts[3].split(' ').join('').split('\u001b')[1].split(';;')[1].split(' ').join('');
    isActive = true;
  } catch (e) {
    url = 'http://' + req.params.symfonykey + '.' + symfonyProxiesConf.tld;
  }

  symfonyProxiesConf.statuses[req.params.symfonykey] = {
    url: url,
    isActive: isActive
  };

  res.json(symfonyProxiesConf);
});

router.get('/stop/:symfonykey', function(req, res, next) {
  const symfonyProxiesConf = getCurrentConfiguration();
  const command = 'symfony server:stop --dir=' + symfonyProxiesConf.domains[req.params.symfonykey];

  execSync(command);

  symfonyProxiesConf.statuses[req.params.symfonykey] = {
    url: 'http://' + req.params.symfonykey + '.' + symfonyProxiesConf.tld,
    isActive: false
  };

  res.json(symfonyProxiesConf);
});

router.post('/run-command', function(req, res, next) {
  if (!req.body.command) {
    res.send('No command provided.');
    return;
  }

  if (!req.body.directory) {
    res.send('No directory provided.');
    return;
  }

  const output = execSync(req.body.command, {
    cwd: req.body.directory
  }, function(error, stdout, stderr) {
    console.log(error, stdout, stderr);
  }).toString();

  res.json(output);
});

router.post('/new-symfony', function(req, res, next) {
  if (!req.body.name) {
    res.send('No name provided.');
    return;
  }

  if (!req.body.directory) {
    res.send('No directory provided.');
    return;
  }

  const command = 'symfony proxy:domain:attach ' + req.body.name + ' --dir=' + req.body.directory;

  execSync(command, {
    cwd: '/tmp'
  });

  res.json(getCurrentConfiguration().statuses[req.body.name].url);
});

module.exports = router;
